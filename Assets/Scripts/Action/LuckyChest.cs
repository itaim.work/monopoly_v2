﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class LuckyChest : ActionNodeRandomFunction
{


    // Start is called before the first frame update
    void Start()
    {
        UIManagerScript = GameObject.Find("Canvas").GetComponent<UIManager>();

        player = GameObject.Find("Player").GetComponent<PlayerController>();

        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        PlayerQuests = new List<Action>();


        PlayerQuests.Add(() => TransectMoneyFromPlayerToBank(player.gameObject, 50)); // back pays you 50$
        PlayerQuests.Add(() => MoveToPos(player.gameObject, 0, "move to START")); // move to TO GO
    }

    public void CallFunction()
    {
        int rnd = UnityEngine.Random.Range(0, PlayerQuests.Count);
        PlayerQuests[rnd].Invoke();

    }




}
