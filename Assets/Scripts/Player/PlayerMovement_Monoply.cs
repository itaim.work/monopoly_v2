﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class PlayerMovement_Monoply : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;

    [SerializeField] private GameManager GameManagerScript;

    private int pos = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

        agent = gameObject.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void move(int value)
    {

        pos += value;

        if (pos >= GameManagerScript.getWayPoints().Length)
        {
            pos -= GameManagerScript.getWayPoints().Length;
        }

        agent.SetDestination(GameManagerScript.getWayPoints()[pos].transform.position);

    }

    public void SetPos(int pos)
    {
        this.pos = pos;
        agent.SetDestination(GameManagerScript.getWayPoints()[pos].transform.position);

    }

    public float GetRemainingDist()
    {
        return agent.remainingDistance;
    }

}
