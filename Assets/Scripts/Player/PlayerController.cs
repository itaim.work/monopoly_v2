﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] protected List<AssetCode> OwnedAssetCodes;
    [SerializeField] protected AssetCode ActiveAsset;
    [SerializeField] protected UIManager UI_Manager;
    [SerializeField] protected float money;
    [SerializeField] protected GameManager GameManagerScript;
    [SerializeField] protected PlayerMovement_Monoply PlayerMovement_Monoply_Script;
    [SerializeField] protected int ID;
    [SerializeField] protected bool FirstRound, IsInPrison;
    [SerializeField] protected string PlayerName;

    // Start is called before the first frame update
    void Start()
    {
        SetStart();

    }


    protected void SetStart()
    {
        PlayerMovement_Monoply_Script = gameObject.GetComponent<PlayerMovement_Monoply>();
        money = 1500;
        UI_Manager = GameObject.Find("Canvas").GetComponent<UIManager>();
        OwnedAssetCodes = new List<AssetCode>();
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        FirstRound = true;
    }


    private void OnTriggerEnter(Collider collision)
    {
        AfterCollision(collision);
    }

    protected void AfterCollision(Collider collision)
    {
        if (PlayerMovement_Monoply_Script.GetRemainingDist() <= 1f && !collision.transform.CompareTag("Player"))
        {
            UI_Manager.ResetUI();
            if (collision.gameObject.CompareTag("Property"))
            {
                ActiveAsset = (AssetCode)collision.gameObject.GetComponent<PropertyAssetCode>();
                UI_Manager.DisplayProperty(collision.gameObject.GetComponent<PropertyAssetCode>());
                TestAsset();
                if (FirstRound)
                {
                    FirstRound = false;
                }
            }
            else if (collision.gameObject.CompareTag("LuckyChest"))
            {
                collision.GetComponent<LuckyChest>().CallFunction();
                if (FirstRound)
                {
                    FirstRound = false;
                }
            }
            else if (collision.gameObject.CompareTag("Tax"))
            {
                int temp = collision.GetComponent<Taxes>().GetTaxAmount();
                this.money -= temp;
                UI_Manager.DisplayAction("landed on taxes, pay " + temp + ".");
                if (FirstRound)
                {
                    FirstRound = false;
                }
            }
            else if (collision.gameObject.CompareTag("TOGO") && !FirstRound)
            {
                this.money += 200;
                UI_Manager.DisplayAction("landed on TOGO, Get 200.");

            }
            else if (collision.gameObject.CompareTag("Cop"))
            {
                IsInPrison = true;
                PlayerMovement_Monoply_Script.SetPos(10);
                UI_Manager.DisplayAction("Landed on cop, Go To Prison.");
            }
            if (OwnedAssetCodes.Count > 0)
            {
                UI_Manager.FillOwnedAssetDisplay(this.OwnedAssetCodes);
            }
            CheckEndTurn();
        }
    }

    public void CheckEndTurn()
    {
        bool flag = true;
        if (money > 0)
        {
            UI_Manager.SetEndTurnButton(true);
        }
        else
        {
            for (int i = 0; i < OwnedAssetCodes.Count; i++)
            {
                if (!OwnedAssetCodes[i].GetIsMortgaged())
                {
                    flag = false;
                    UI_Manager.SetNoMoneyForEndText(true);
                }
            }
            if (flag)
            {
                UI_Manager.SetEndGamePerPlayerButton(true);
            }
        }
    }
    public void EndGame()
    {
        foreach (var item in OwnedAssetCodes)
        {
            if (item.gameObject.CompareTag("Property"))
            {
                PropertyAssetCode property = (PropertyAssetCode)item;
                property.ResetAsset();
            }
            else
            {
                item.ResetAsset();
            }
        }
        GameManagerScript.EndTurn();
        UI_Manager.SetEndGamePerPlayerButton(false);
        UI_Manager.SetNoMoneyForEndText(false);
    }
    protected void TestAsset()
    {
        if (!ActiveAsset.getIsOwned())
        {
            if (ActiveAsset.GetPrice() <= this.money)
            {
                UI_Manager.EnablePurchase();
            }
            else
            {
                UI_Manager.NoMoney();
            }
        }
        else
        {
            if (ActiveAsset.GetOwnerID() == this.ID)
            {
                if (ActiveAsset.gameObject.CompareTag("Property"))
                {
                    PropertyAssetCode activeProperty = (PropertyAssetCode)ActiveAsset;
                    if (activeProperty.GetHousePrice() <= this.money && activeProperty.GetIsCollectionFull())
                    {
                        UI_Manager.SetBuyHouseButton(true);
                    }
                }
            }
            else if (!ActiveAsset.GetIsMortgaged())
            {
                GameManagerScript.TransectMoneyBetweenPlayers(ID, ActiveAsset.GetOwnerID(), ActiveAsset.calcFee());
            }
        }
    }
    public void BuyAsset()
    {
        OwnedAssetCodes.Add(ActiveAsset);
        ActiveAsset.setOwner(ID);
        this.money -= ActiveAsset.GetPrice();
        UI_Manager.FillOwnedAssetDisplay(this.OwnedAssetCodes);

    }
    public float GetMoney()
    {
        return money;
    }
    public void SetMoney(float amount)
    {
        this.money = amount;
    }
    public string GetName()
    {
        return this.PlayerName;
    }
    public int GetID()
    {
        return this.ID;
    }
    public List<AssetCode> GetOwnedAssetCodes()
    {
        return this.OwnedAssetCodes;
    }
    public AssetCode GetActiveAsset()
    {
        return this.ActiveAsset;
    }
    public void BuyHouse()
    {
        PropertyAssetCode activeProperty = (PropertyAssetCode)ActiveAsset;
        activeProperty.BuyHouse();
    }

    public void SetIsInPrison(bool value)
    {
        this.IsInPrison = value;
    }
    public bool GetIsInPrison()
    {
        return this.IsInPrison;
    }
}
