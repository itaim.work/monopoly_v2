﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private GameObject[] WayPoints;
    [SerializeField] private string[] Collections;
    [SerializeField] private PropertyAssetCode[] PropertyAssetCodes;
    [SerializeField] public List<GameObject> Players;
    [SerializeField] private int ActivePlayer, doubleCounter = 0, InPrisonCounter = 3;
    [SerializeField] private UIManager UIManagerScript;
    int cube1, cube2 = 0;


    // Start is called before the first frame update
    void Start()
    {
        UIManagerScript = GameObject.Find("Canvas").GetComponent<UIManager>();
        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");
        Array.Sort(WayPoints, SortByName);
        GameObject[] temp = GameObject.FindGameObjectsWithTag("Property");
        PropertyAssetCodes = new PropertyAssetCode[temp.Length];
        for (int i = 0; i < temp.Length; i++)
        {
            PropertyAssetCodes[i] = temp[i].GetComponent<PropertyAssetCode>();
        }
        ActivePlayer = UnityEngine.Random.Range(0, Players.Count);
        UIManagerScript.SetTurnText("Turn: " + Players[ActivePlayer].GetComponent<PlayerController>().GetName() + ".");
    }

    // Update is called once per frame
    void Update()
    {

    }

    int SortByName(GameObject a, GameObject b)
    {
        return a.name.CompareTo(b.name);
    }

    public GameObject[] getWayPoints()
    {
        return WayPoints;
    }

    public string getCollectionByID(int id)
    {
        return Collections[id];
    }
    public PropertyAssetCode[] GetPropertyAssetCodes()
    {
        return PropertyAssetCodes;
    }
    public string GetPlayerNameByID(int id)
    {
        return Players[id].GetComponent<PlayerController>().GetName();
    }

    public void Roll()
    {
        cube1 = UnityEngine.Random.Range(1, 3);
        cube2 = UnityEngine.Random.Range(1, 3);
        if (cube2 == cube1)
        {
            doubleCounter++;
        }
        if(doubleCounter == 3)
        {
            GetActivePlayerController().SetIsInPrison(true);
            Players[ActivePlayer].GetComponent<PlayerMovement_Monoply>().SetPos(10);
        }
        Players[ActivePlayer].GetComponent<PlayerMovement_Monoply>().move(cube1 + cube2);
        UIManagerScript.SetCubeResultText(cube1, cube2);
        UIManagerScript.SetRollButton(false);
    }

    public void EndTurn()
    {
        doubleCounter = 0;
        int counter = 0;
        do
        {
            ActivePlayer++;
            if (ActivePlayer == Players.Count)
            {
                ActivePlayer = 0;
            }
            if (GetActivePlayerController().GetIsInPrison())
            {
                if(InPrisonCounter <= 0)
                {
                    InPrisonCounter = 3;
                    GetActivePlayerController().SetIsInPrison(false);
                }
                else
                {
                    InPrisonCounter--;
                }
            }
        }
        while (GetActivePlayerController().GetMoney() <= 0 || GetActivePlayerController().GetIsInPrison());
        UIManagerScript.ResetUI();
        UIManagerScript.SetRollButton(true);
        UIManagerScript.SetEndTurnButton(false);
        UIManagerScript.SetTurnText("Turn: " + Players[ActivePlayer].GetComponent<PlayerController>().GetName() + ".");
        foreach (GameObject item in Players)
        {
            if (item.GetComponent<PlayerController>().GetMoney() > 0)
            {
                counter++;
            }
        }
        if (counter <= 1)
        {
            UIManagerScript.OpenWinMenu();
        }
    }

    public void TransectMoneyBetweenPlayers(int source, int dest, int amount)
    {
        Players[source].GetComponent<PlayerController>().SetMoney(Players[source].GetComponent<PlayerController>().GetMoney() - amount);
        Players[dest].GetComponent<PlayerController>().SetMoney(Players[dest].GetComponent<PlayerController>().GetMoney() + amount);

    }

    public PlayerController GetActivePlayerController()
    {
        return Players[ActivePlayer].GetComponent<PlayerController>();
    }
}
