﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu, levelMenu;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Quit()
    {
        Application.Quit();
    }
    public void ChooseLevel()
    {
        mainMenu.SetActive(false);
        levelMenu.SetActive(true);
    }

    public void PlayLevel(int index)
    {
        SceneManager.LoadScene(index);
    }

}
